void main() {
  var winningApps = [
    "FNB Banking", //2012
    "Snapscan", //2013
    "Live-Inspect", //2014
    "Wumdrop", //2015
    "Domestly", //2016
    "Standard Bank Swift", //2017
    "Khula Ecosystems", //2018
    "Naked Insurance", //2019
    "Easy Equities", //2020
    "Ambani Africa App" //2021
  ];

  print("The winners were $winningApps"); // a)
  print("The winner in 2017 was " + winningApps[5]); // b)
  print("The winner in 2018 was " + winningApps[6]);
  print(winningApps.length); //c)
}
