void main() {
  var application = new App();

  application.name = "Ambani Africa";
  application.category = "Best Educational App";
  application.developer = "Mukundi Lambani";
  application.year = 2021;

  print(application.name); //objects that print out the details of class
  print(application.category);
  print(application.developer);
  print(application.year);
  application.printAppDetails();
  application.printAppCaps(); // function that transforms the app name
}

class App {
  String? name;
  String? category;
  String? developer;
  int? year;
  void printAppCaps() {
    print("$name".toUpperCase());
  }

  void printAppDetails() {
    print("App name is $name");
    print("The category is $category");
    print("The developer is $developer");
    print("The app was the winner for $year");
  }
}
